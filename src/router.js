import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home'
import Products from './views/Products'
import Construction from './components/Construction'
import About from './components/About'
import Contact from './components/Contact'
import LeftNav from './components/LeftNav'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/products/:product',
      name: 'products',
      components: {
        default: Products,
        leftNav: LeftNav
      },
      props: {
        default: true,
        leftNav: true
      }
    },
    {
      path: '/construction',
      name: 'construction',
      component: Construction
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
  ],
  mode: 'history'
})

