FROM node as builder
WORKDIR /usr/local/build
COPY src/ src/
COPY public/ public/
COPY package.json ./
RUN npm install
RUN npm run build

FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /usr/local/build/dist /euro-web/
